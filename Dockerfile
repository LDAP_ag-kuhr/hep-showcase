ARG PYTHON_VERSION=3.10
FROM python:${PYTHON_VERSION}

WORKDIR /usr/src/app

COPY . .

RUN pip install --upgrade pip
RUN pip install --no-cache-dir -r requirements.txt
# create directory to mount data if needed
RUN mkdir data

EXPOSE 8888
CMD ["jupyter", "notebook", "scikit-hep.ipynb", "--ip=0.0.0.0", "--no-browser", "--allow-root"]
